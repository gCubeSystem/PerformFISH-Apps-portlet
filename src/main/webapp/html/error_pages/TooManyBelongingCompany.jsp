<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<div class="alert alert-error">
  <h4>There is an issue in your autorisations</h4>
  It seems you belong to more than one company. <strong>You must belong to one company only to use this application.</strong>
</div>
