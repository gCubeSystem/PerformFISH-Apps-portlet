package org.gcube.portlets.user.performfish.util.comparators;

import java.util.Comparator;

import org.gcube.common.storagehubwrapper.shared.tohl.WorkspaceItem;


public class WSItemComparator implements Comparator<WorkspaceItem> {
	@Override
	public int compare(WorkspaceItem o1, WorkspaceItem o2) {

		return o1.getName().compareTo(o2.getName());

	}
}


