# Changelog for PerformFISH-Apps-portlet

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1-5-0-SNAPSHOT] - 2022-02-01

- Feature #21319 revised and ported to the new auth framework

- New creations and removal of companies is not allowed in this new version.

## [v1-4-0] - 2021-06-14

- [Feature #21319] (https://support.d4science.org/issues/21319) Removed Home Library dependency and Ported to StorageHub 
 
## [v1-3-1] - 2019-10-09

- Fix for Bug #16599, Validation portlet not dealing with angular parenthesis signs
- Feature #16621 Validation portlet: users should be warned if run analysis is performed on non up-to-data anonymised data
- Enabled annual data analysis</Change>
 
## [v1-2-0] - 2018-12-18

- UUID Field Valorisation upon new Association, Company and 	Farms creation

## [v1-0-0] - 2018-10-10

- First release

